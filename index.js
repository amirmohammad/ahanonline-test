const express = require('express');
const app = express(); 
require('dotenv').config(); 
const port = process.env.PORT || 8181; 

const dummy = require('./dummy');


// main route
app.get('/', (req, res) => {
    const people = dummy.people; 

    // converting output format
    let info = [];
    for( let p of people){
        let dummyInfo =  `${p.firstname} ${p.lastname} : ${p.phoneNumber}`
        info.push(dummyInfo)
    }

    return console.log(info) 

    // return res.json(info);

})

app.listen(port, () => {
    console.log(`Server started on ${port}`);
});