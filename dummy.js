
const data = {
    people: [
        {
            firstname: "reza",
            lastname: "motahari",
            phoneNumber: '+989745612303'
        },
        {
            firstname: "ali",
            lastname: "alavi",
            phoneNumber: '+989123456789'
        },
        {
            firstname: "minoo",
            lastname: "nazari",
            phoneNumber: '+989363654556'
        },
        {
            firstname: "hosein",
            lastname: "hafezian",
            phoneNumber: '+989212345678'
        },
        {
            firstname: "parsa",
            lastname: "kiarsi",
            phoneNumber: '+989257894631'
        },
        {
            firstname: "meysam",
            lastname: "taheri nia",
            phoneNumber: 'no phone'
        },

    ]
}

module.exports = data;